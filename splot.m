#! /usr/bin/octave -qf

% pkg load instrument-control;              % Necessary if instrument-control is locally installed

args = argv;
if (rows(args) == 2)
    port = cell2mat(args(1));
    baudrate = str2num(cell2mat(args(2)));
    printf("Connecting to %s using baudrate of %d\n",port, baudrate);
else
    % Try to find either USB or ACM in available ports
    available_ports = serialportlist;
    res = strfind(available_ports,"USB");
    portindex = find(cellfun(@isempty,res) == 0);
    if isempty(portindex)
        res = strfind(available_ports,"ACM");
        portindex = find(cellfun(@isempty,res) == 0);
        if isempty(portindex)
            printf("No suitable ports found, exiting octave \n")
            exit
        endif
    endif
    port = cell2mat(available_ports(portindex))
    printf("Not enough arguments, using %s and baudrate of 115200 \n",port)
    baudrate = 115200;
endif

hold on;
grid on;

%% Make the plot is bit more clear
set(groot, 'defaultlinelinewidth', 2)
set(groot, 'defaultaxesxminortick', 'on')
set(groot, 'defaultaxesyminortick', 'on');
set(groot, 'defaulttextfontsize', 16); 
set(groot, "defaultaxesfontsize", 14) 


%% Connect to serial port and flush, initalize sample string
s1 = serial(port, baudrate);
%s1 = serial("/dev/ttyACM0", 38400);                % This is for testing
srl_flush(s1);
sample = '';

%% Read one line here to figure out how many signals there are (delimited by TAB)
while true
    data = srl_read(s1,1);                           % Read one character at a time
    if (data != 10)                                  % If LF (end of Serial.println)
       sample = strcat(sample,char(data));
    else
        values = strsplit (sample, "\t");            % split sample by TABs, strsplit returns cellarray
        num_signals = size(values)(2);
        sample = '';                                 % Reset sample string
        break;
    endif
endwhile

x = linspace (0, 1000, 1000);                       % Create the x-axis vector
buffSize = 1000;
sampleBuff = zeros(num_signals,buffSize);

for i=1:num_signals
    plot(nan);                                                  % Initialize empty plots
endfor

p = findobj(gca,'Type','line');                                 % get handle (vector of the line objects)

for i=1:num_signals
    set (p(i), "xdata", x);                                     % Set the x-axis to the sample index
endfor

unwind_protect
srl_flush(s1);
    while true
        data = srl_read(s1,1);                                   % Read one character at a time
        if (data != 10)                                          % If LF (end of Serial.println)
           sample = strcat(sample,char(data));
        else 
            values = strsplit(sample,"\t");
            valvec = cellfun(@str2num,values);
            sample = '';                                         % Reset sample string
            sampleBuff = [valvec' sampleBuff(:,1:end-1)];
            for i=1:num_signals
                set (p(i), "ydata", sampleBuff(i,:));
                drawnow
            endfor
        endif
    endwhile
unwind_protect_cleanup
    srl_close(s1);
    fprintf ('Caught interrupt. Click figure to export data or use menu to save\n');
    [x, y, buttons] = ginput (1);
    if buttons == 1
        filename = strcat("splotdata_",datestr (now(),30),".csv")
        csvwrite(filename,sampleBuff')
    endif
end
