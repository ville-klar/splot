# 📈 SPLOT 📈

Simple plotting utility for serial data streams.
Read accompanying [blog post](https://villeklar.com/post/2020/08/31/octave-serial-plotter/) for more details.

Requires [octave](https://www.gnu.org/software/octave/) and [the instrument control package](https://octave.sourceforge.io/instrument-control/).
Only tested on Linux.
